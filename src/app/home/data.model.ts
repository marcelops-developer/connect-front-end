export interface DataModel {
  indicator?: Indicator;
  country?: Country;
  countryiso3code?: string;
  date?: string;
  value?: any;
  unit?: string;
  obs_status?: string;
  decimal?: number;
}

interface Indicator {
  id: string;
  value: string;
}

interface Country {
  id: string;
  value: string;
}
