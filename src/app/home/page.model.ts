export interface PageModel {
  page?: string;
  pages?: string;
  per_page?: string;
  total?: string;
  sourceid?: string;
  lastupdated?: string;
}
