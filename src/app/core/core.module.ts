import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeService } from '../home/home.service';
import { SharedModule } from '@shared/shared.module';

@NgModule({
    declarations: [
    ],
    imports: [
        CommonModule,
        NgbModule,
        SharedModule
    ],
    exports: [
    ],
    providers: [
        HomeService
    ]
})
export class CoreModule { }
